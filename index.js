const express = require("express");
const app = express();
const dotenv = require("dotenv");
const mongoose = require("mongoose");

//Import Routes
const authRoutes = require("./routes/auth");

const posts = require("./routes/posts");

dotenv.config();

//Connect to DB
mongoose.connect(
  process.env.DB_CONECT,
  {
    useNewUrlParser: true,
    useUnifiedTopology: true
  },
  () => console.log("connect to db!")
);

//Middleware
app.use(express.json());

//Routes Middlewares
app.use("/api/user", authRoutes);
app.use("/api/posts",posts);

app.listen(3000, () => console.log("Server Up and Running"));
