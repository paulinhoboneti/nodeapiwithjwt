// myscript.js
// This example uses Node 8's async/await syntax.

const oracledb = require("oracledb");

oracledb.outFormat = oracledb.OUT_FORMAT_OBJECT;

const mypw = "ckf"; // set mypw to the hr schema password

async function run() {
  let connection;

  try {
    connection = await oracledb.getConnection({
      user: "ckf_user",
      password: mypw,
      connectString: "ora-dv16:1521/dv16"
    });

    const result = await connection.execute(
      `SELECT * FROM CKF_DBC_TAB_ORDEM_CT`
      //,      [103] // bind value for :id
    );
    console.log(result.rows);
  } catch (err) {
    console.error(err);
  } finally {
    if (connection) {
      try {
        await connection.close();
      } catch (err) {
        console.error(err);
      }
    }
  }
}

run();
